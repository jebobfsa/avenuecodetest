- How to compile and run the application with an example for each call.

*compile using maven command: mvn clean install
*run :
	*option 1: by commandline mvn spring-boot:run
	*option 2: after compile, exec java -jar test-0.0.1-SNAPSHOT.jar, using the .jar file in the target folder
	*option 3: run class TestApplication.java, using commandline or some IDE

*the file AvenueCodeTest.postman_collection.json can be imported by Postman, and following tests can be run all the created features.
*follow the steps below:
	*Save parent product
	*Save product
	*get all product
	*get all product without-dependencies
	*get by id without-dependencies
	*get by id
	*get by parent id
	*get images by product id

- How to run the suite of automated tests.
*run :
	*using maven command: mvn test
	*run java class TestProductControllerTests.java, using commandline or some IDE

- Mention anything that was asked but not delivered and why, and any additional comments. 
*improve tests


