package com.avenuecode.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.entity.Image;
import com.avenuecode.entity.Product;
import com.avenuecode.repository.ProductRepository;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductRepository repository;
	
	// a)Get all products excluding relationships (child products, images
	@RequestMapping(method = RequestMethod.GET, value = "/without-dependencies")
    public ResponseEntity<Collection<Product>> getAllProductsWithoutDependencies(){
		return new ResponseEntity<>(repository.findAllWithoutDependencies(), HttpStatus.OK);
    }
	
	// b)Get all products including specified relationships (child product and/or images
	@RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<Product>> getAllProducts(){
        return new ResponseEntity<>((Collection<Product>) repository.findAll(), HttpStatus.OK);
    }
	
	// c)Same as 1 using specific product identity (excluding relationships)
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/without-dependencies")
	public ResponseEntity<Product> getProductByIdWithoutDependencies(@PathVariable Long id) {
		return new ResponseEntity<>(repository.findOneWithoutDependencies(id),HttpStatus.OK);
	}
	
	// d)Same as 2 using specific product identity (including specified relationships)
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable Long id) {
		return new ResponseEntity<>(repository.findOne(id),HttpStatus.OK);
	}
	
	// e)Get set of child products for specific product
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/child")
    public ResponseEntity<Collection<Product>> getAllChildProducts(@PathVariable Long id){
        return new ResponseEntity<>((Collection<Product>) repository.findAllChildProducts(id), HttpStatus.OK);
    }
	
	// f)Get set of images for specific product
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/images")
    public ResponseEntity<Collection<Image>> getAllImagesByProduct(@PathVariable Long id){
        return new ResponseEntity<>((Collection<Image>) repository.findAllImagesByProduct(id), HttpStatus.OK);
    }

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> addProduct(@RequestBody Product input) {
		return new ResponseEntity<>(repository.save(input), HttpStatus.CREATED);
	}
	
}
