package com.avenuecode.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.avenuecode.entity.Image;
import com.avenuecode.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

	@Query("select NEW Product(p.id, p.description) from Product p")
	Collection<Product> findAllWithoutDependencies();
	
	@Query("select NEW Product(p.id, p.description) from Product p where p.id = ?1 ")
	Product findOneWithoutDependencies(@Param("id") Long id);
	
	@Query("select p from Product p where p.parentProduct.id = ?1 ")
	Collection<Product> findAllChildProducts(@Param("id") Long id);
	
	@Query("select p.images from Product p where p.id = ?1 ")
	Collection<Image> findAllImagesByProduct(@Param("id") Long id);
	
}
