package com.avenuecode;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.avenuecode.entity.Image;
import com.avenuecode.entity.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.DEFINED_PORT)
public class TestProductControllerTests {

	private RestTemplate restTemplate = new RestTemplate();
	
	@Test
	public void testInsertProducts(){
		
		Product p1 = new Product();
		p1.setDescription("Parent product 1");
		HttpEntity<Product> requestUpdate = new HttpEntity<>(p1, null);
		ResponseEntity<Product> p1Saved = restTemplate.exchange("http://localhost:8080/products",HttpMethod.PUT, requestUpdate, Product.class);
		assertEquals(p1Saved.getStatusCode(), HttpStatus.CREATED);
		
		Product p2 = new Product();
		p2.setParentProduct(p1Saved.getBody());
		p2.setDescription("product 1");
		
		List<Image> images = new ArrayList<>();
		Image i1 = new Image("image 1", "www.avenuecode.com/image1");
		Image i2 = new Image("image 2", "www.avenuecode.com/image2");
		images.add(i1);
		images.add(i2);
		p2.setImages(images);
		
		requestUpdate = new HttpEntity<>(p2, null);
		ResponseEntity<Product> p2Saved = restTemplate.exchange("http://localhost:8080/products",HttpMethod.PUT, requestUpdate, Product.class);
		assertEquals(p2Saved.getStatusCode(), HttpStatus.CREATED);
		assertEquals(2, p2Saved.getBody().getImages().size());
		
	}
	
	// a)Get all products excluding relationships (child products, images
	@Test
	public void testGetAllProductsExcludingRelationships(){
		List<Product> response = restTemplate.getForObject("http://localhost:8080/products/without-dependencies", List.class);
		assertEquals(2, response.size());
	}
	
	// b)Get all products including specified relationships (child product and/or images
	@Test
	public void testGetAllProductsIncludingRelationships(){
		
	}
			 
	// c)Same as 1 using specific product identity
	@Test
	public void testGetAllProductsExcludingRelationshipsById(){
		
	}
			
	// d)Same as 2 using specific product identity
	@Test
	public void testGetAllProductsIncludingRelationshipsById(){
		
	}
			 
	// e)Get set of child products for specific product
	@Test
	public void testGetChildProductsById(){
		
	}
			
	// f)Get set of images for specific product
	@Test
	public void testGetIProductsById(){
		
	}
	
}
